# ARTIFICIAL INTELLIGENCE - EXAM

## MAIN GOAL

Your goal is to train the (simple) CNN model on the data you have available (**exam_data.zip** above).

After the training process (you can save the model easily using *.save*), evaluate the model on validation data (in the *valid* folder) and save the results to a *.csv* file according to the form in the following image:

![Hamilton](results.png)

## Dataset

You have a dataset of 10 classes:

1. casseteplayer
2. chainsaw
3. church
4. englishspringer
5. frenchhorn
6. garbagetruck
7. gaspump
8. golfball
9. parachute
10. tench

Finally, send the csv file to ivan.cik@tuke.sk and sign up for the Teams conversation to defend the code.
